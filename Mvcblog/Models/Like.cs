namespace Mvcblog.Database
{
    public class Like
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string UserEmail { get; set; }
    }
}