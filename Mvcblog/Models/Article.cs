﻿
namespace Mvcblog.Database
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; } 
        public string UserName { get; set; }
        public long Date { get; set; }
        public string ImagePath { get; set; }    
        public bool IsPrivate { get; set; }
    }
}
