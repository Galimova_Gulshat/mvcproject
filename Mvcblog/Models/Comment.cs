using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Mvcblog.Database
{
    public class Comment
    {
        public int Id { get; set; }
        
        public string Text { get; set; }

        public string UserEmail { get; set; }
        public User User { get; set; }

        public int PostId { get; set; }
        public Article Post { get; set; }
    }
}