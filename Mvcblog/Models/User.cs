﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mvcblog.Database
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string CodePhrase { get; set; }
        public string ImagePath { get; set; }  
        public List<Article> Articles { get; set; }
    }
}
