namespace Mvcblog.Markdown
{
    public interface IMarkdownParser
    {
        string Parse(string markdown, bool stripScriptTags = true);
    }
}