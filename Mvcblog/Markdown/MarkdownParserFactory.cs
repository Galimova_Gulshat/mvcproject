namespace Mvcblog.Markdown
{
    public static class MarkdownParserFactory
    {
        public static string DefaultMarkdownParserName { get;  } = "MarkDig";

        public static IMarkdownParser CurrentParser;

        public static IMarkdownParser GetParser(bool usePragmaLines = false,
            bool forceLoad = false)                                                
        {
            if (!forceLoad && CurrentParser != null)
                return CurrentParser;
            
            CurrentParser = new MarkdownParserMarkdig(usePragmaLines, forceLoad);
            return CurrentParser;
        }  
    }   
}