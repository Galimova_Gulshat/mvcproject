using System;
using System.IO;
using Markdig;
using Markdig.Extensions.AutoIdentifiers;
using Markdig.Extensions.EmphasisExtras;
using Markdig.Renderers;

namespace Mvcblog.Markdown
{
    public class  MarkdownParserMarkdig : MarkdownParserBase
    {
        public static MarkdownPipeline Pipeline;

        public static bool StripScriptCode { get; set; } = true;

        private readonly bool _usePragmaLines;

        public static Action<MarkdownPipelineBuilder> ConfigurePipelineBuilder { get; set; }

        public MarkdownParserMarkdig(bool usePragmaLines = false, bool force = false, Action<MarkdownPipelineBuilder> markdigConfiguration = null)
        {

            _usePragmaLines = usePragmaLines;
            if (force || Pipeline == null)
            {
                if (markdigConfiguration == null && ConfigurePipelineBuilder != null)                
                    markdigConfiguration = ConfigurePipelineBuilder;

                var builder = CreatePipelineBuilder(markdigConfiguration);                
                Pipeline = builder.Build();
            }
        }
  
        public override string Parse(string markdown, bool sanitizeHtml = true)
        {
            if (string.IsNullOrEmpty(markdown))
                return string.Empty;

            string html;
            using (var htmlWriter = new StringWriter())
            {
                var renderer = CreateRenderer(htmlWriter);

                Markdig.Markdown.Convert(markdown, renderer, Pipeline);

                html = htmlWriter.ToString();
            }
            html = ParseFontAwesomeIcons(html);                   
            return html;
        }

        public virtual MarkdownPipelineBuilder CreatePipelineBuilder(Action<MarkdownPipelineBuilder> markdigConfiguration)
        {
            MarkdownPipelineBuilder builder = null;

            if (markdigConfiguration == null)
            {
                builder = new MarkdownPipelineBuilder()
                    .UseEmphasisExtras()
                    .UsePipeTables()
                    .UseGridTables()
                    .UseFooters()
                    .UseFootnotes()
                    .UseCitations()
                    .UseAutoLinks() 
                    .UseAutoIdentifiers(AutoIdentifierOptions.GitHub) 
                    .UseAbbreviations()
                    .UseYamlFrontMatter()
                    .UseEmojiAndSmiley(true)
                    .UseMediaLinks()
                    .UseListExtras()
                    .UseFigures()
                    .UseTaskLists()
                    .UseCustomContainers()
                    .UseGenericAttributes();          

                if (_usePragmaLines)
                    builder = builder.UsePragmaLines();

                return builder;
            }
            
            builder = new MarkdownPipelineBuilder();
            markdigConfiguration.Invoke(builder);

            if (_usePragmaLines)
                builder = builder.UsePragmaLines();
            
            return builder;
        }

        protected virtual IMarkdownRenderer CreateRenderer(TextWriter writer)
        {
            return new HtmlRenderer(writer);
        }
    }
}