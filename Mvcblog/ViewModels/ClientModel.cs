using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mvcblog.ViewModels
{
    public class ClientModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        
        [Display(Name = "Имя")]
        public string Name { get; set; }
        
        [ScaffoldColumn(false)]
        public string CodePhrase { get; set; }
        
        [Display(Name = "Email")]
        public string Login { get; set; }
        
        [ScaffoldColumn(false)]
        [StringLength(10, MinimumLength = 6, ErrorMessage = "Длина строки должна быть от 6 до 10 символов")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [ScaffoldColumn(false)]
        public string ImagePath { get; set; }  
        
        [ScaffoldColumn(false)]
        public List<ArticleModel> Articles { get; set; }
    }
}