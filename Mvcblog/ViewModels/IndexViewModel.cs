using System.Collections.Generic;

namespace Mvcblog.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<ArticleModel> Articles { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}