using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Mvcblog.ViewModels
{
    public class ArticleModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Название")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Поле должно быть установлено")]
        [Display(Name = "Содержание")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        
        [Display(Name = "Создатель")]
        public string UserName { get; set; }

        [Display(Name = "Дата")]
        public long Date { get; set; }

        public string ImagePath { get; set; }
        
    }
}