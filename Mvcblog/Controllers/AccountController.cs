using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mvcblog.ViewModels; 
using Mvcblog.Database; 
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Mvcblog.Controllers
{
    public class AccountController : Controller
    {
        private readonly DbManager _databaseManager;
        
        public AccountController(DbManager manager) => _databaseManager = manager;
        
        public ActionResult Delete(int id)
        {
            _databaseManager.Delete(id, User.Identity.Name);
            return RedirectToAction("Profile", "Account");
        }
        
        [Authorize]
        [HttpGet]
        public ActionResult Profile()
        {
            var articles = _databaseManager
                .GetPersonArticles(User.Identity.Name)
                .Select(MappingModels.MapDatabaseArticle);
            var currentUser = _databaseManager.GetUser(User.Identity.Name);
            ViewData["User"] = MappingModels.MapUserToClient(currentUser);
            return View(articles);
        }
        
        [HttpGet]
        [Route("OtherProfile/{email}")]
        [Route("[controller]/[action]/{email}")]
        public ActionResult OtherProfile(string email)
        {
            var articles = _databaseManager
                .GetPersonArticles(email).Where(a => !a.IsPrivate)
                .Select(MappingModels.MapDatabaseArticle);
            var currentUser = _databaseManager.GetUser(email);
            ViewData["User"] = MappingModels.MapUserToClient(currentUser);
            return View(articles);
        }

        public ActionResult Article(ArticleModel articleModel) => View(articleModel);
        
        public ActionResult Create() => View();
        
        [HttpPost]
        public ActionResult Create(ArticleModel articleModel, IFormFile imageFile, string isPrivate)
        {
            if (!ModelState.IsValid) return View();
            articleModel.UserName = User.Identity.Name;
            articleModel.Date = DateTime.Now.ToBinary();
            var privateBool = isPrivate != null && !isPrivate.Equals("public");
            
            if (imageFile != null && imageFile.Length > 0)
            {
                var fileName = imageFile.FileName;
                var extension = Path.GetExtension(fileName);
                if (extension == ".png" || extension == ".jpg")
                {
                    var filePath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images"));
                    using (var fileStream = new FileStream(Path.Combine(filePath, fileName), FileMode.Create))
                    {
                        imageFile.CopyTo(fileStream);
                    }

                    articleModel.ImagePath = "~/images/"+ fileName;
                }
            }
            
            _databaseManager.AddArticle(MappingModels.MapModelsArticle(articleModel), privateBool);
            
            return RedirectToAction("Profile", "Account");
        }
        
        [HttpGet]
        public ActionResult EditUser()
        {
            var user = MappingModels.MapUserToClient(_databaseManager.GetUser(User.Identity.Name));
            return View(user);
        }
        
        [HttpPost]
        public ActionResult EditUser(ClientModel clientModel, IFormFile imageFile)
        {
            if (!ModelState.IsValid) return View();
            if (imageFile != null && imageFile.Length > 0)
            {
                var fileName = imageFile.FileName;
                var extension = Path.GetExtension(fileName);
                if (extension == ".png" || extension == ".jpg")
                {
                    var filePath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images"));
                    using (var fileStream = new FileStream(Path.Combine(filePath, fileName), FileMode.Create))
                    {
                        imageFile.CopyTo(fileStream);
                    }

                    clientModel.ImagePath = "~/images/"+ fileName;
                }
            }
            
            _databaseManager.UpdateUser(MappingModels.MapClientToUser(clientModel));
            
            return RedirectToAction("Profile");
        }
        
        public ActionResult Edit(int id) => View(MappingModels.MapDatabaseArticle(_databaseManager.GetPersonArticle(id)));
        
        [HttpPost]
        public ActionResult Edit(ArticleModel articleModel)
        {
            if (!ModelState.IsValid) return View();
            
            _databaseManager.EditArticle(MappingModels.MapModelsArticle(articleModel));
            
            return RedirectToAction("Profile", "Account");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Auth");
        }
    }
}