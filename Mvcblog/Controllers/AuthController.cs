using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Mvcblog.Database;
using Mvcblog.ViewModels;

namespace Mvcblog.Controllers
{
    public class AuthController : Controller
    {
        private readonly DbManager _databaseManager;
        
        public AuthController(DbManager manager) => _databaseManager = manager;
        
        [HttpGet]
        public IActionResult Login() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid) return View(model);
            
            if (_databaseManager.IsHasUser(model.Email, model.Password))
            {
                await Authenticate(model.Email); 

                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            
            return View(model);
        }
        
        [HttpGet]
        public IActionResult Register() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid) return View(model);
            
            if (_databaseManager.HasEmail(model.Email))
            {
                _databaseManager.Registration(model.Email, model.Password, model.CodePhrase);

                await Authenticate(model.Email);
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> ResetPassword()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> ResetPassword(RegisterModel model)
        {
            if (!ModelState.IsValid) return View();
            if (!_databaseManager.HasEmailWithCodePhrase(model.Email, model.CodePhrase) 
               || model.Password != model.ConfirmPassword) return View();
                
            _databaseManager.UpdatePassword(model.Email, model.Password);
            
            return View("Login");
        }

        private async Task Authenticate(string userName)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };

            var id = new ClaimsIdentity(
                claims,
                "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(id));
        }
    }
}