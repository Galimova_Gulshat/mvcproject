﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Mvcblog.Models;
using Mvcblog.Database;
using Mvcblog.ViewModels;

namespace Mvcblog.Controllers
{
    public class HomeController : Controller
    {     
        private readonly DbManager _databaseManager;
        
        public HomeController(DbManager manager) => _databaseManager = manager;
        
        public IActionResult Index(int page=1)
        {
            const int pageSize = 7;
             
            var source = _databaseManager
                .GetArticles()
                .Where(a => !a.IsPrivate)
                .Select(MappingModels.MapDatabaseArticle)
                .ToList();
            
            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();
 
            var pageViewModel = new PageViewModel(count, page, pageSize);
            var viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                Articles = items
            };
            return View(viewModel);
        }
        
        [HttpGet]
        public IActionResult Show(int id)
        {
            ViewData["Comments"] = _databaseManager.GetComments(id);          
            var post = MappingModels.MapDatabaseArticle(_databaseManager.GetPersonArticle(id));
            ViewData["Article"] = post;
            ViewData["LikesCount"] = _databaseManager.GetLikesCount(id);
            return post == default(ArticleModel) ? View("Index") : View("Article", post);
        }
        
        [HttpPost]
        public IActionResult Comment(string content, int articleId)
        {       
           _databaseManager.AddComment(articleId, User.Identity.Name, content);
           ViewData["Comments"] = _databaseManager.GetComments(articleId);
           return View("Article", MappingModels.MapDatabaseArticle(_databaseManager.GetPersonArticle(articleId)));
        }

        public ActionResult Article(ArticleModel articleModel) => View(articleModel);
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
            => View(new ErrorModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        
        [Authorize]
        [HttpPost]
        public IActionResult Like(int id)
        {
            var resultString = _databaseManager.LikePost(id, User.Identity.Name);
            return new JsonResult(resultString);
        }
    }
}
