using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Mvcblog.Models;

namespace Mvcblog.Database
{
    public class DbManager
    {        
        private BlogContext _context;

        public DbManager(BlogContext context) => _context = context;
        
        public bool IsHasUser(string email, string password)
        {
            var isLogin = _context.Users.FirstOrDefault(u =>
                u.Login == email && u.Password == password);
            return isLogin != null;
        }
        
        public bool HasEmail(string email)
        {
            var isLogin = _context.Users.FirstOrDefault(u =>
                u.Login == email);
            return isLogin == null;   
        }
        
        public bool HasEmailWithCodePhrase(string email, string codePhrase)
        {
            var isLogin = _context.Users.FirstOrDefault(u =>
                u.Login == email && u.CodePhrase == codePhrase);
            return isLogin != null;
        }
        
        
        public void UpdatePassword(string email, string password)
        {
            var user = _context.Users.FirstOrDefault(u => u.Login == email);
            if (user == default(User))
                return;
            user.Password = password;
            _context.SaveChanges();
            
        }
        
        public void AddArticle(Article article, bool isPrivate)
        {
            article.IsPrivate = isPrivate;
            _context.Articles.Add(article); 
            _context.SaveChanges();     
        }

        public User GetUser(string login)
        {
            var user = _context.Users.FirstOrDefault(u => u.Login == login);
            return user;
        }

        public void UpdateUser(User newUser)
        {
            var user = _context.Users.FirstOrDefault(u => u.Login == newUser.Login);
            if (user == null) return;
            user.Name = newUser.Name;
            _context.SaveChanges();
            
        }

        public Article GetPersonArticle(int id) => _context.Articles.FirstOrDefault(a => a.Id == id);
        
        public List<Article> GetPersonArticles(string username)
        {
            var articles = _context.Articles.Where(a => a.UserName == username);
            return articles.OrderByDescending(article => article.Date).ToList();     
        }
        
        public List<Article> GetArticles()
        {
            var retCount = 100;
            var count = _context.Articles.Count();
            return count < retCount 
                ? _context.Articles.OrderByDescending(article => article.Date).ToList() 
                : _context.Articles.Skip(count - retCount).OrderByDescending(article => article.Date).ToList();
            
        }
        
        public void Registration( string login, string password, string codePhrase)
        {
            _context.Users.Add(new User() {Login = login, Password = password, CodePhrase = codePhrase});
            _context.SaveChanges();
            
        }

        public  void Delete(int id, string userName)
        {
            var article = _context.Articles.FirstOrDefault(a => a.Id == id && a.UserName == userName);
            if (article == default(Article)) return;
            _context.Articles.Remove(article);
            _context.SaveChanges();
        }

        public void AddComment(int postId, string userId, string content)
        {
            _context.Comments.Add(new Comment() {PostId = postId, UserEmail = userId, Text = content});
            _context.SaveChanges();
        }
        
        public IEnumerable<Comment> GetComments(int articleId) => _context.Comments.Where(c => c.PostId == articleId);

        public void EditArticle(Article mapModelsArticle)
        {
            var article = _context.Articles.FirstOrDefault(a => a.Id == mapModelsArticle.Id);
            if (article == default(Article)) return;
            article.Name = mapModelsArticle.Name;
            article.Content = mapModelsArticle.Content;
            _context.SaveChanges();
        }

        public string LikePost(int postId, string userEmail)
        {
            var isLiked = false;
            var article = _context.Articles.FirstOrDefault(a => a.Id == postId);
            var user = _context.Users.FirstOrDefault(u => u.Login == userEmail);
            if (default(Article) == article || default(User) == user) return "";

            var userLike = _context.Likes.FirstOrDefault(like => like.PostId == postId && like.UserEmail == userEmail);
            if (default(Like) == userLike)
            {
                var like = new Like { PostId = postId, UserEmail = userEmail};
                isLiked = true;
                _context.Likes.Add(like);
            }
            else
            {
                _context.Likes.Remove(userLike);
            }

            _context.SaveChanges();
            return (!isLiked ? "👍 " : "✅ ") + GetLikesCount(postId);
        }

        public int GetLikesCount(int postId) => _context.Likes.Count(l => l.PostId == postId);
    }
}