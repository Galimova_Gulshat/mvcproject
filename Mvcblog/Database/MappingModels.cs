using System;
using System.Collections.Generic;
using System.Linq;
using Mvcblog.ViewModels;

namespace Mvcblog.Database
{
    public static class MappingModels
    {
        public static ClientModel MapUserToClient(User user)
        {
            List<ArticleModel> articles;
            if (user.Articles != null)
                articles = user.Articles.Select(a => MapDatabaseArticle(a)).ToList();
            else articles = null;
            return new ClientModel()
            {
                Name = user.Name,
                Login = user.Login,
                Id = user.Id,
                Password = user.Password,
                Articles = articles,
                CodePhrase = user.CodePhrase,
                ImagePath = user.ImagePath
            };
        }
        
        public static User MapClientToUser(ClientModel user)
        {
            List<Article> articles;
            if (user.Articles != null)
                articles = user.Articles.Select(a => MapModelsArticle(a)).ToList();
            else articles = null;
            return new User()
            {
                Name = user.Name,
                Login = user.Login,
                Id = user.Id,
                Password = user.Password,
                Articles = articles,
                CodePhrase = user.CodePhrase,
                ImagePath = user.ImagePath
            };
        }

        public static ArticleModel MapDatabaseArticle(Article article)
        {
            if (article == null)
                return null;
            return new ArticleModel()
            {
                Id = article.Id,
                Name = article.Name,
                Content = article.Content,
                UserName = article.UserName,
                Date = article.Date,
                ImagePath = article.ImagePath
            };
        }
        
        public static Article MapModelsArticle(ArticleModel articleModel)
        {
            if (articleModel == null)
                return null;
            return new Article()
            {
                Id = articleModel.Id,
                Name = articleModel.Name,
                Content = articleModel.Content,
                UserName = articleModel.UserName, 
                Date = articleModel.Date,
                ImagePath = articleModel.ImagePath
            };
        }
    }
}